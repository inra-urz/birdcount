from tkinter import BOTTOM, BooleanVar, Tk, Label, Button, Menu, messagebox, Radiobutton, Toplevel, getboolean
from tkinter import Canvas, N, W, S, E, Grid, CENTER, Spinbox, StringVar, IntVar, DoubleVar
from tkinter.messagebox import showinfo
from tkinter.filedialog import askopenfilename, asksaveasfilename
from xmlrpc.client import Boolean
import cv2
from PIL import Image,ImageTk
import numpy as np

#==============================================================================#
class App(Tk):
    #--------------------------------------------------------------------------#
    def __init__(self):
        super().__init__()
        self.create_menu_bar()
        self.create_widget()

        # config Grid
        for i in range(12):
            Grid.rowconfigure(self, i, weight=1) 
            Grid.columnconfigure(self, i, weight=1) 

        # configure the root window
        self.title('Detection App')

        # control variable
        self.do_process = False
        self.dot_size = 1
        self.poly = []
        self.imgsavepoly = []

        # variables
        self.delay = 10

        # button
        """ self.button = Button(self, text='Click Me')
        self.button['command'] = self.button_clicked
        self.button.pack() """

        self.update()
    #--------------------------------------------------------------------------#
    def create_menu_bar(self):
        menu_bar = Menu(self)

        menu_file = Menu(menu_bar, tearoff=0)
        menu_file.add_command(label="Open Image", command=self.open_image)
        menu_file.add_command(label="Open Video", command=self.open_video)
        menu_file.add_command(label="Open Camera", command=self.open_camera)
        menu_file.add_separator()
        menu_file.add_command(label="Save Image", command=self.save_multi_img)
        menu_file.add_separator()
        menu_file.add_command(label="Exit", command=self.quit)
        menu_bar.add_cascade(label="File", menu=menu_file)

        self.play = BooleanVar()
        self.play.set(False)
        self.simpleblobdetection = BooleanVar()
        self.simpleblobdetection.set(False)
        self.hsvdetection = BooleanVar()
        self.hsvdetection.set(False)

        self.dotSize1 = BooleanVar()
        self.dotSize1.set(True)
        self.dotSize2 = BooleanVar()
        self.dotSize2.set(False)
        self.dotSize5 = BooleanVar()
        self.dotSize5.set(False)
        self.dotSize10 = BooleanVar()
        self.dotSize10.set(False)

        self.setPoly = BooleanVar()
        self.setPoly.set(False)
        self.drawPoly = BooleanVar()
        self.drawPoly.set(False)

        # add a submenu
        sub_menu = Menu(menu_bar, tearoff=0)
        sub_menu.add_checkbutton(label="1", onvalue=1, offvalue=0, 
                                    variable=self.dotSize1, command=self.dot_size1)
        sub_menu.add_checkbutton(label="2", onvalue=1, offvalue=0, 
                                    variable=self.dotSize2, command=self.dot_size2)
        sub_menu.add_checkbutton(label="5", onvalue=1, offvalue=0, 
                                    variable=self.dotSize5, command=self.dot_size5)
        sub_menu.add_checkbutton(label="10", onvalue=1, offvalue=0, 
                                    variable=self.dotSize10, command=self.dot_size10)

        menu_config = Menu(menu_bar, tearoff=0)
        menu_config.add_checkbutton(label="Play", onvalue=1, offvalue=0, 
                                    variable=self.play, command=self.check_play_video)
        menu_config.add_separator()
        menu_config.add_checkbutton(label="Set Polygone", onvalue=1, offvalue=0, 
                                    variable=self.setPoly, command=self.set_polygone)
        menu_config.add_checkbutton(label="Draw Polygone", onvalue=1, offvalue=0, 
                                    variable=self.drawPoly, command=self.draw_polygone)
        menu_config.add_separator()
        menu_config.add_checkbutton(label="Simple Blob Detection", onvalue=1, offvalue=0, 
                                    variable=self.simpleblobdetection, command=self.check_simple_blob_detection)
        menu_config.add_checkbutton(label="HSV Detection", onvalue=1, offvalue=0, 
                                    variable=self.hsvdetection, command=self.check_hsv_detection)
        menu_config.add_separator()
        # add the File menu to the menubar
        menu_config.add_cascade(label="Dot Size",menu=sub_menu)
        menu_bar.add_cascade(label="Config", menu=menu_config)

        menu_help = Menu(menu_bar, tearoff=0)
        menu_help.add_command(label="About", command=self.do_about)
        menu_bar.add_cascade(label="Help", menu=menu_help)

        self.config(menu=menu_bar)
    #--------------------------------------------------------------------------#
    def set_polygone(self):
        if self.setPoly.get():
            print("check")
            self.canvas.bind("<Button1-Motion>", self.do_something)
        else:
            self.canvas.bind("<Button1-Motion>", self.MouseMove)
    #--------------------------------------------------------------------------#
    def draw_polygone(self):
        if len(self.poly)>=3:
            mask = np.zeros((self.img.shape[0], self.img.shape[1]))
            cv2.fillConvexPoly(mask, np.array(self.poly), 1)
            mask = mask.astype(bool)

            out = np.zeros_like(self.img)
            out[mask] = self.img[mask]

            self.imgsavepoly.append(out)
            self.img_resize = self.imgsavepoly[-1]
            self.img = self.imgsavepoly[-1].copy()

            self.fit_canvas(self.img_resize)

            self.poly = []
            self.drawPoly.set(False)
            del self.x1r
            del self.x2r
            del self.y1r
            del self.y2r

            self.setPoly.set(False)
            self.set_polygone()
    #--------------------------------------------------------------------------#
    def create_widget(self):
        # canvas
        self.canvas = Canvas(self)
        self.canvas.grid(row=0, column=0, rowspan=12, columnspan=12, 
                         sticky=N+S+E+W)
        
        self.canvas.bind("<Button-1>", self.MouseDown)
        self.canvas.bind("<Button1-Motion>", self.MouseMove)
        self.canvas.bind("<ButtonRelease-1>", self.MouseRelease)
        self.canvas.bind("<Button-3>", self.MouseDown2)
    #--------------------------------------------------------------------------#
    def open_image(self):
        self.f = askopenfilename(title="Choose the file to open",
                                 filetypes=[("JPG image", ".jpg"),
                                            ("PNG image", ".png"), 
                                            ("GIF image", ".gif")])
        if self.f != '':
            self.delete_media()
            # load img
            self.img = cv2.imread(self.f)
            # copy img to process
            self.img_resize = self.img.copy()
            # resize and add in canvas
            self.fit_canvas(self.img)
        
        del self.f
    #--------------------------------------------------------------------------#
    def save_image(self,namepath,img):
        if hasattr(self,'img'):
            try:
                cv2.imwrite(namepath,img)
                return "saved"

            except FileNotFoundError:
                return "cancelled"
    #--------------------------------------------------------------------------#
    def open_video(self):
        self.f = askopenfilename(title="Choose the file to open",
                                 filetypes=[("AVI image", ".avi"), 
                                            ("MP4 image", ".mp4")])
        if self.f != '':
            self.delete_media()
            self.cap = cv2.VideoCapture(self.f)
            # capture first frame
            ret, self.img = self.cap.read()
            # check if frame exist
            if ret:
                # copy img to process
                self.img_resize = self.img.copy()
                # resize and add in canvas
                self.fit_canvas(self.img)
            else:
                del self.cap
        
        del self.f
    #--------------------------------------------------------------------------#
    def open_camera(self):
        self.delete_media()
        self.cap = cv2.VideoCapture(0)
        # capture frame
        ret, self.img = self.cap.read()
        # check if frame exist
        if ret:
            # copy img to process
            self.img_resize = self.img.copy()
            # resize and add in canvas
            self.fit_canvas(self.img)
        else:
            del self.cap
    #--------------------------------------------------------------------------#
    def check_play_video(self):
        if self.play.get():
            self.play.set(hasattr(self,'cap'))
    #--------------------------------------------------------------------------#
    def delete_media(self):
        if hasattr(self,'cap'):
            del self.cap
            self.check_play_video()
    #--------------------------------------------------------------------------#
    def dim_resize_ratio(self, img):
        h, w, c = img.shape
        r = w / h
        hc = self.canvas.winfo_height()
        wc = self.canvas.winfo_width()
        if (int(hc * r) > wc):
            self.w = wc
            self.h = int(wc / r)
        else:
            self.w = int(hc * r)
            self.h = hc
    #--------------------------------------------------------------------------#
    def fit_canvas(self,img):
        # set img size ratio
        self.dim_resize_ratio(img)
        # transform from BGR to RGB
        self.img2 = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        # resize
        self.img2 = cv2.resize(self.img2, (self.w, self.h))
        # resize and convert to fit canvas
        self.img2 = ImageTk.PhotoImage(Image.fromarray(self.img2))
        # get canvas center
        wc = int(self.canvas.winfo_width() / 2)
        hc = int(self.canvas.winfo_height() / 2)
        # add image to the canvas items
        self.canvas.create_image(wc,hc,anchor=CENTER,image=self.img2)
    #--------------------------------------------------------------------------#
    def check_simple_blob_detection(self):
        if self.simpleblobdetection.get():
            self.hsvdetection.set(False)
            # create params widget
            self.simple_blob_detection_widget()
        else:
            self.create_widget()
            del self.model
    #--------------------------------------------------------------------------#
    def simple_blob_detection(self, img):
        self.set_simple_blob_detector_params()
        # pre-process img
        imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        median = cv2.medianBlur(imgGray, self.blur)
        kernel1 = np.ones((self.erode, self.erode), np.uint8)
        dst = cv2.erode(median, kernel1)
        kernel2 = np.ones((self.dilate, self.dilate), np.uint8)
        dst = cv2.dilate(dst, kernel2)
        cv2.imshow('image',dst)
        # Detect blobs.
        keypoints = self.model.detect(dst)
        # Draw detected blobs as red circles.
        # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
        self.img_resize = cv2.drawKeypoints(img, keypoints, np.zeros((1, 1)), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    #--------------------------------------------------------------------------#
    def simple_blob_detection_widget(self):
        self.params = cv2.SimpleBlobDetector_Params()
        self.erode = 1
        self.dilate = 1
        self.blur = 1

        # canvas
        self.canvas.grid(row=0, column=0, rowspan=12, columnspan=10, 
                         sticky=N+S+E+W)

        # Spinbox
        minArea = IntVar(value=0)
        self.minArea_spinbox = Spinbox(self,from_=0,to=100000,textvariable=minArea,wrap=True, command=self.process)
        self.minArea_spinbox.grid(row=1, column=10, sticky=N)
        
        maxArea = IntVar(value=100000)
        self.maxArea_spinbox = Spinbox(self,from_=0,to=100000,textvariable=maxArea,wrap=True, command=self.process)
        self.maxArea_spinbox.grid(row=1, column=11, sticky=N)

        minCircularity = DoubleVar(value=0)
        self.minCircularity_spinbox = Spinbox(self,format="%.2f", increment=0.01,from_=0,to=1,textvariable=minCircularity,wrap=True, command=self.process)
        self.minCircularity_spinbox.grid(row=3, column=10, sticky=N)
        
        maxCircularity = DoubleVar(value=1)
        self.maxCircularity_spinbox = Spinbox(self,format="%.2f", increment=0.01,from_=0,to=1,textvariable=maxCircularity,wrap=True, command=self.process)
        self.maxCircularity_spinbox.grid(row=3, column=11, sticky=N)
        
        minConvexity = DoubleVar(value=0)
        self.minConvexity_spinbox = Spinbox(self,format="%.2f", increment=0.01,from_=0,to=1,textvariable=minConvexity,wrap=True, command=self.process)
        self.minConvexity_spinbox.grid(row=5, column=10, sticky=N)
        
        maxConvexity = DoubleVar(value=1)
        self.maxConvexity_spinbox = Spinbox(self,format="%.2f", increment=0.01,from_=0,to=1,textvariable=maxConvexity,wrap=True, command=self.process)
        self.maxConvexity_spinbox.grid(row=5, column=11, sticky=N)
        
        minInertiaRatio = DoubleVar(value=0)
        self.minInertiaRatio_spinbox = Spinbox(self,format="%.2f", increment=0.01,from_=0,to=1,textvariable=minInertiaRatio,wrap=True, command=self.process)
        self.minInertiaRatio_spinbox.grid(row=7, column=10, sticky=N)
        
        maxInertiaRatio = DoubleVar(value=1)
        self.maxInertiaRatio_spinbox = Spinbox(self,format="%.2f", increment=0.01,from_=0,to=1,textvariable=maxInertiaRatio,wrap=True, command=self.process)
        self.maxInertiaRatio_spinbox.grid(row=7, column=11, sticky=N)
        
        erodeValue = IntVar(value=1)
        self.erodeValue_spinbox = Spinbox(self,from_=1,to=50,textvariable=erodeValue,wrap=True, command=self.process)
        self.erodeValue_spinbox.grid(row=9, column=10, sticky=N)
        
        dilateValue = IntVar(value=1)
        self.dilateValue_spinbox = Spinbox(self,from_=1,to=50,textvariable=dilateValue,wrap=True, command=self.process)
        self.dilateValue_spinbox.grid(row=9, column=11, sticky=N)
        
        blurValue = IntVar(value=1)
        self.blurValue_spinbox = Spinbox(self,from_=1,to=21, increment=2, textvariable=blurValue,wrap=True, command=self.process)
        self.blurValue_spinbox.grid(row=11, column=10, sticky=N)

        # labels
        self.minArea_label = Label(self, text='min Area')
        self.minArea_label.grid(row=0, column=10, sticky=S)
        
        self.maxArea_label = Label(self, text='max Area')
        self.maxArea_label.grid(row=0, column=11, sticky=S)
        
        self.minCircularity_label = Label(self, text='min Circularity')
        self.minCircularity_label.grid(row=2, column=10, sticky=S)
        
        self.maxCircularity_label = Label(self, text='max Circularity')
        self.maxCircularity_label.grid(row=2, column=11, sticky=S)
        
        self.minConvexity_label = Label(self, text='min Convexity')
        self.minConvexity_label.grid(row=4, column=10, sticky=S)
        
        self.maxConvexity_label = Label(self, text='max Convexity')
        self.maxConvexity_label.grid(row=4, column=11, sticky=S)
        
        self.minInertiaRatio_label = Label(self, text='min Inertia Ratio')
        self.minInertiaRatio_label.grid(row=6, column=10, sticky=S)
        
        self.maxInertiaRatio_label = Label(self, text='max Inertia Ratio')
        self.maxInertiaRatio_label.grid(row=6, column=11, sticky=S)
        
        self.erodeValue_label = Label(self, text='erode value')
        self.erodeValue_label.grid(row=8, column=10, sticky=S)
        
        self.dilateValue_label = Label(self, text='dilate value')
        self.dilateValue_label.grid(row=8, column=11, sticky=S)
        
        self.blurValue_label = Label(self, text='blur value')
        self.blurValue_label.grid(row=10, column=10, sticky=S)
        
        self.objectCountValue_label = Label(self, text='Objects count : 0')
        self.objectCountValue_label.grid(row=10, column=11, rowspan=2)

        self.set_simple_blob_detector_params()
    #--------------------------------------------------------------------------#
    def set_simple_blob_detector_params(self):
        self.params.filterByArea = True
        if self.minArea_spinbox.get()=='':
            self.params.minArea = 0
        else:
            self.params.minArea = int(self.minArea_spinbox.get())
        if self.maxArea_spinbox.get()=='':
            self.params.maxArea = 0
        else:
            self.params.maxArea = int(self.maxArea_spinbox.get())

        self.params.filterByCircularity = True
        if self.minCircularity_spinbox.get()=='':
            self.params.minCircularity = 0.0
        else:
            self.params.minCircularity = float(self.minCircularity_spinbox.get())
        if self.maxCircularity_spinbox.get()=='':
            self.params.minCircularity = 0
        else:
            self.params.maxCircularity = float(self.maxCircularity_spinbox.get())

        self.params.filterByConvexity = True
        if self.minConvexity_spinbox.get()=='':
            self.params.minConvexity = 0
        else:
            self.params.minConvexity = float(self.minConvexity_spinbox.get())
        if self.maxConvexity_spinbox.get()=='':
            self.params.maxConvexity = 0
        else:
            self.params.maxConvexity = float(self.maxConvexity_spinbox.get())

        self.params.filterByInertia = True
        if self.minInertiaRatio_spinbox.get()=='':
            self.params.minInertiaRatio = 0
        else:
            self.params.minInertiaRatio = float(self.minInertiaRatio_spinbox.get())
        if self.maxInertiaRatio_spinbox.get()=='':
            self.params.maxInertiaRatio = 0
        else:
            self.params.maxInertiaRatio = float(self.maxInertiaRatio_spinbox.get())
            
        if self.erodeValue_spinbox.get()=='':
            self.erode = 1
        else:
            self.erode = int(self.erodeValue_spinbox.get())

        if self.dilateValue_spinbox.get()=='':
            self.dilate = 1
        else:
            self.dilate= int(self.dilateValue_spinbox.get())
            
        if self.blurValue_spinbox.get()=='':
            self.blur = 1
        else:
            self.blur = int(self.blurValue_spinbox.get())

        self.model = cv2.SimpleBlobDetector_create(self.params)       
    #--------------------------------------------------------------------------#
    def check_hsv_detection(self):
        if self.hsvdetection.get():
            self.simpleblobdetection.set(False)
            self.create_widget()
            # create hsv widget
            self.hsv_detection_widget()
        else:
            self.create_widget()
    #--------------------------------------------------------------------------#
    def hsv_detection_widget(self):
        self.params = [0,1000,0,179,0,255,0,255,1,1,1]

        # canvas
        self.canvas.grid(row=0, column=0, rowspan=12, columnspan=10, 
                         sticky=N+S+E+W)

        # Spinbox
        minArea = IntVar(value=0)
        self.minArea_spinbox = Spinbox(self,from_=0,to=10000,textvariable=minArea,wrap=True, command=self.process)
        self.minArea_spinbox.grid(row=1, column=10, sticky=N)
        
        maxArea = IntVar(value=10000)
        self.maxArea_spinbox = Spinbox(self,from_=0,to=10000,textvariable=maxArea,wrap=True, command=self.process)
        self.maxArea_spinbox.grid(row=1, column=11, sticky=N)

        minHue = IntVar(value=0)
        self.minHue_spinbox = Spinbox(self,from_=0,to=179,textvariable=minHue,wrap=True, command=self.process)
        self.minHue_spinbox.grid(row=3, column=10, sticky=N)
        
        maxHue = IntVar(value=179)
        self.maxHue_spinbox = Spinbox(self,from_=0,to=179,textvariable=maxHue,wrap=True, command=self.process)
        self.maxHue_spinbox.grid(row=3, column=11, sticky=N)
        
        minSaturation = IntVar(value=0)
        self.minSaturation_spinbox = Spinbox(self,from_=0,to=255,textvariable=minSaturation,wrap=True, command=self.process)
        self.minSaturation_spinbox.grid(row=5, column=10, sticky=N)
        
        maxSaturation = IntVar(value=255)
        self.maxSaturation_spinbox = Spinbox(self,from_=0,to=255,textvariable=maxSaturation,wrap=True, command=self.process)
        self.maxSaturation_spinbox.grid(row=5, column=11, sticky=N)
        
        minValue = IntVar(value=0)
        self.minValue_spinbox = Spinbox(self,from_=0,to=255,textvariable=minValue,wrap=True, command=self.process)
        self.minValue_spinbox.grid(row=7, column=10, sticky=N)
        
        maxValue = IntVar(value=255)
        self.maxValue_spinbox = Spinbox(self,from_=0,to=255,textvariable=maxValue,wrap=True, command=self.process)
        self.maxValue_spinbox.grid(row=7, column=11, sticky=N)
        
        erodeValue = IntVar(value=1)
        self.erodeValue_spinbox = Spinbox(self,from_=1,to=50,textvariable=erodeValue,wrap=True, command=self.process)
        self.erodeValue_spinbox.grid(row=9, column=10, sticky=N)
        
        dilateValue = IntVar(value=1)
        self.dilateValue_spinbox = Spinbox(self,from_=1,to=50,textvariable=dilateValue,wrap=True, command=self.process)
        self.dilateValue_spinbox.grid(row=9, column=11, sticky=N)
        
        blurValue = IntVar(value=1)
        self.blurValue_spinbox = Spinbox(self,from_=1,to=21, increment=2, textvariable=blurValue,wrap=True, command=self.process)
        self.blurValue_spinbox.grid(row=11, column=10, sticky=N)

        # labels
        self.minArea_label = Label(self, text='min Area')
        self.minArea_label.grid(row=0, column=10, sticky=S)
        
        self.maxArea_label = Label(self, text='max Area')
        self.maxArea_label.grid(row=0, column=11, sticky=S)
        
        self.minHue_label = Label(self, text='min Hue')
        self.minHue_label.grid(row=2, column=10, sticky=S)
        
        self.maxHue_label = Label(self, text='max Hue')
        self.maxHue_label.grid(row=2, column=11, sticky=S)
        
        self.minSaturation_label = Label(self, text='min Saturation')
        self.minSaturation_label.grid(row=4, column=10, sticky=S)
        
        self.maxSaturation_label = Label(self, text='max Saturation')
        self.maxSaturation_label.grid(row=4, column=11, sticky=S)
        
        self.minValue_label = Label(self, text='min Value')
        self.minValue_label.grid(row=6, column=10, sticky=S)
        
        self.maxValue_label = Label(self, text='max Value')
        self.maxValue_label.grid(row=6, column=11, sticky=S)
        
        self.erodeValue_label = Label(self, text='erode value')
        self.erodeValue_label.grid(row=8, column=10, sticky=S)
        
        self.dilateValue_label = Label(self, text='dilate value')
        self.dilateValue_label.grid(row=8, column=11, sticky=S)
        
        self.blurValue_label = Label(self, text='blur value')
        self.blurValue_label.grid(row=10, column=10, sticky=S)
        
        self.objectCountValue_label = Label(self, text='Objects count : 0')
        self.objectCountValue_label.grid(row=10, column=11, rowspan=2)
    #--------------------------------------------------------------------------#
    def set_hsv_detector_params(self):
        if self.minArea_spinbox.get()=='':
            self.params[0] = 0
        else:
            self.params[0] = int(self.minArea_spinbox.get())
        if self.maxArea_spinbox.get()=='':
            self.params[1] = 0
        else:
            self.params[1] = int(self.maxArea_spinbox.get())

        if self.minHue_spinbox.get()=='':
            self.params[2] = 0
        else:
            self.params[2] = int(self.minHue_spinbox.get())
        if self.maxHue_spinbox.get()=='':
            self.params[3] = 0
        else:
            self.params[3] = int(self.maxHue_spinbox.get())

        if self.minSaturation_spinbox.get()=='':
            self.params[4] = 0
        else:
            self.params[4] = int(self.minSaturation_spinbox.get())
        if self.maxSaturation_spinbox.get()=='':
            self.params[5] = 0
        else:
            self.params[5] = int(self.maxSaturation_spinbox.get())

        if self.minValue_spinbox.get()=='':
            self.params[6] = 0
        else:
            self.params[6] = int(self.minValue_spinbox.get())
        if self.maxValue_spinbox.get()=='':
            self.params[7] = 0
        else:
            self.params[7] = int(self.maxValue_spinbox.get())
        
        if self.erodeValue_spinbox.get()=='':
            self.params[8] = 1
        else:
            self.params[8] = int(self.erodeValue_spinbox.get())
        if self.dilateValue_spinbox.get()=='':
            self.params[9] = 1
        else:
            self.params[9] = int(self.dilateValue_spinbox.get())
            
        if self.blurValue_spinbox.get()=='':
            self.params[10] = 1
        else:
            self.params[10] = int(self.blurValue_spinbox.get())
    #--------------------------------------------------------------------------#
    def hsv_detection(self,img):
        self.set_hsv_detector_params()
        # Set minimum and maximum HSV values to display
        lower = np.array([self.params[2], self.params[4], self.params[6]], np.uint8)
        upper = np.array([self.params[3], self.params[5], self.params[7]], np.uint8)

        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(hsv, lower, upper)
        imghsv = cv2.bitwise_and(img, img, mask=mask)

        imgGray = cv2.cvtColor(imghsv, cv2.COLOR_BGR2GRAY)
        median = cv2.medianBlur(imgGray, self.params[10])

        th, dst = cv2.threshold(median,0,255, cv2.THRESH_BINARY)
        kernel1 = np.ones((self.params[8], self.params[8]), np.uint8)
        dst = cv2.erode(dst, kernel1)
        kernel2 = np.ones((self.params[9], self.params[9]), np.uint8)
        dst = cv2.dilate(dst, kernel2)

        cv2.imshow('dst',cv2.resize(dst,(640,480)))

        self.getContours(dst, img)
    #--------------------------------------------------------------------------#
    def getContours(self,dst,img):
        contours, hierarchy = cv2.findContours(dst, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        keypoints = img.copy()
        count = 0

        for cnt in contours:
            area = cv2.contourArea(cnt)
            if self.params[0] < area < self.params[1]:
                peri = cv2.arcLength(cnt, True)
                approx = cv2.approxPolyDP(cnt, 0.02 * peri, True)
                x, y, w, h = cv2.boundingRect(approx)

                # cv2.rectangle(keypoints, (x, y), (x + w, y +h), (0, 0, 255), 2)
                cv2.circle(keypoints, (int(x+w/2),int(y+h/2)), radius=self.dot_size, color=(0, 0, 255), thickness=-1)

                count += 1

        self.objectCountValue_label.configure(text='Objects count : '+str(count))
        
        self.img_resize = keypoints

        return count
    #--------------------------------------------------------------------------#
    def process(self):
        self.do_process = not self.do_process
    #--------------------------------------------------------------------------#
    def MouseDown(self, event):
        "Quand on clique gauche"
        self.currObject =None
        # event.x et event.y contiennent les coordonnées du clic effectué
        self.x1, self.y1 = event.x, event.y
        # add poly points
        self.x1r = int(((self.x1 - ((self.canvas.winfo_width() - self.w) / 2)) * self.img.shape[1]) / self.w)
        self.y1r = int(((self.y1 - ((self.canvas.winfo_height() - self.h) / 2)) * self.img.shape[0]) / self.h)
        if (0 <= self.x1r <= self.img.shape[1]) and (0 <= self.y1r <= self.img.shape[0]):
            self.poly.append([self.x1r,self.y1r])
            print(self.poly)
            # draw line
            if len(self.poly)>=2:
                self.imgsavepoly.append(self.img_resize.copy())
                self.x1r = self.poly[-2][0]
                self.y1r = self.poly[-2][1]
                self.x2r = self.poly[-1][0]
                self.y2r = self.poly[-1][1]
                # Draw a diagonal blue line with thickness of 5 px
                cv2.line(self.img_resize,(self.x1r,self.y1r),(self.x2r,self.y2r),(255,0,0),5)
    #--------------------------------------------------------------------------#
    def MouseMove(self, event):
        "Quand on bouge la souris bouton gauche enfoncé"
        self.x2, self.y2 = event.x, event.y
        #self.dx, self.dy = self.x3 -self.x1, self.y3 -self.y1
        self.Position = list()
        if self.x1 < self.x2 :
            self.Position.append(self.x1)
            if self.y1 < self.y2 :
                self.Position.append(self.y1)
                self.Position.append(self.x2)
                self.Position.append(self.y2)
            else :
                self.Position.append(self.y2)
                self.Position.append(self.x2)
                self.Position.append(self.y1)
        else :
            self.Position.append(self.x2)
            if self.y1 < self.y2 :
                self.Position.append(self.y1)
                self.Position.append(self.x1)
                self.Position.append(self.y2)
            else :
                self.Position.append(self.y2)
                self.Position.append(self.x1)
                self.Position.append(self.y1)
 
        try :
            self.canvas.delete(self.rectangle)
            self.rectangle = self.canvas.create_rectangle(self.Position[0], self.Position[1],\
                                                  self.Position[2], self.Position[3], \
                                                  outline='red', width=2, state="normal")
        except :
            self.rectangle = self.canvas.create_rectangle(self.Position[0], self.Position[1],\
                                                  self.Position[2], self.Position[3], \
                                                  outline='red', width=2, state="normal")
    #--------------------------------------------------------------------------#
    def MouseRelease(self, event):
        if hasattr(self, 'img') and hasattr(self,'x2') and hasattr(self,'y2'):
            if self.x2-self.x1 != 0 and self.y2-self.y1 != 0:
                self.x1r = int(((self.x1 - ((self.canvas.winfo_width() - self.w) / 2)) * self.img.shape[1]) / self.w)
                self.y1r = int(((self.y1 - ((self.canvas.winfo_height() - self.h) / 2)) * self.img.shape[0]) / self.h)
                self.x2r = int(((self.x2 - ((self.canvas.winfo_width() - self.w) / 2)) * self.img.shape[1]) / self.w)
                self.y2r = int(((self.y2 - ((self.canvas.winfo_height() - self.h) / 2)) * self.img.shape[0]) / self.h)
                self.img_crop = self.img[self.y1r:self.y2r,self.x1r:self.x2r]
                if self.img_crop.shape[0] != 0 and self.img_crop.shape[1] != 0:
                    self.img_resize = self.img_crop.copy()
                else:
                    del self.img_crop
                    self.img_resize = self.img.copy()
                self.canvas.delete("all")
    #--------------------------------------------------------------------------#
    def MouseDown2(self, event):
        if hasattr(self, 'img_crop'):
            if len(self.imgsavepoly)>0:
                self.img_resize = self.imgsavepoly[-1]
            else:
                self.img_resize = self.img.copy()
            del self.x1r
            del self.x2r
            del self.y1r
            del self.y2r
            del self.img_crop
        elif len(self.imgsavepoly)>0:
            self.img_resize = self.imgsavepoly[-1]
            self.imgsavepoly.pop()
            self.poly.pop()
        else:
            self.imgsavepoly.pop()
            self.poly.pop()
            self.poly.pop()
            del self.x1r
            del self.x2r
            del self.y1r
            del self.y2r
    #--------------------------------------------------------------------------#
    def save_multi_img(self):
        if hasattr(self,'img'):

            file_path = asksaveasfilename (title="Save result",
                                            filetypes=[("JPG image", ".jpg"),
                                                    ("PNG image", ".png"), 
                                                    ("GIF image", ".gif")],
                                            defaultextension=".jpg")
            self.save_image(file_path[:-4]+"_init"+file_path[-4:],self.img)

            if self.simpleblobdetection.get():
                img = self.img.copy()

                self.set_simple_blob_detector_params()
                # pre-process img
                imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                median = cv2.medianBlur(imgGray, self.blur)
                kernel1 = np.ones((self.erode, self.erode), np.uint8)
                dst = cv2.erode(median, kernel1)
                kernel2 = np.ones((self.dilate, self.dilate), np.uint8)
                dst = cv2.dilate(dst, kernel2)
                # Detect blobs.
                keypoints = self.model.detect(dst)
                # Draw detected blobs as red circles.
                # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
                contour = cv2.drawKeypoints(img, keypoints, np.zeros((1, 1)), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

                self.save_image(file_path[:-4]+"_gray"+file_path[-4:],imgGray)
                self.save_image(file_path[:-4]+"_median"+file_path[-4:],median)
                self.save_image(file_path[:-4]+"_dst"+file_path[-4:],dst)
                self.save_image(file_path[:-4]+"_contours"+file_path[-4:],contour)
                                


            elif self.hsvdetection.get():
                img = self.img.copy()
                    
                self.set_hsv_detector_params()
                # Set minimum and maximum HSV values to display
                lower = np.array([self.params[2], self.params[4], self.params[6]], np.uint8)
                upper = np.array([self.params[3], self.params[5], self.params[7]], np.uint8)

                hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
                mask = cv2.inRange(hsv, lower, upper)
                imghsv = cv2.bitwise_and(img, img, mask=mask)
                imgGray = cv2.cvtColor(imghsv, cv2.COLOR_BGR2GRAY)
                median = cv2.medianBlur(imgGray, self.params[10])
                th, dst = cv2.threshold(median,0,255, cv2.THRESH_BINARY)
                kernel1 = np.ones((self.params[8], self.params[8]), np.uint8)
                dst = cv2.erode(dst, kernel1)
                kernel2 = np.ones((self.params[9], self.params[9]), np.uint8)
                dst = cv2.dilate(dst, kernel2)
                count = self.getContours(dst, img)

                self.save_image(file_path[:-4]+"_hsv"+file_path[-4:],hsv)
                self.save_image(file_path[:-4]+"_mask"+file_path[-4:],mask)
                self.save_image(file_path[:-4]+"_mask_hsv"+file_path[-4:],imghsv)
                self.save_image(file_path[:-4]+"_gray"+file_path[-4:],imgGray)
                self.save_image(file_path[:-4]+"_median"+file_path[-4:],median)
                self.save_image(file_path[:-4]+"_dst"+file_path[-4:],dst)
                self.save_image(file_path[:-4]+"_contours"+file_path[-4:],self.img_resize)

                with open(file_path[:-4]+"_config.txt", 'w') as f:
                    f.write("Min H             : "+str(self.params[2]))
                    f.write("\nMax H             : "+str(self.params[3]))
                    f.write("\nMin S             : "+str(self.params[4]))
                    f.write("\nMax S             : "+str(self.params[5]))
                    f.write("\nMin V             : "+str(self.params[6]))
                    f.write("\nMax V             : "+str(self.params[7]))
                    f.write("\nMin area          : "+str(self.params[0]))
                    f.write("\nMax area          : "+str(self.params[1]))
                    f.write("\nBlur size         : "+str(self.params[10]))
                    f.write("\nErode size        : "+str(self.params[8]))
                    f.write("\nDilate size       : "+str(self.params[9]))
                    f.write("\nBirds count       : "+str(count))
    #--------------------------------------------------------------------------#
    def dot_size1(self):
        if self.dotSize1.get():
            self.dot_size = 1
            self.dotSize2.set(False)
            self.dotSize5.set(False)
            self.dotSize10.set(False)
    #--------------------------------------------------------------------------#
    def dot_size2(self):
        if self.dotSize2.get():
            self.dot_size = 2
            self.dotSize1.set(False)
            self.dotSize5.set(False)
            self.dotSize10.set(False)
    #--------------------------------------------------------------------------#
    def dot_size5(self):
        if self.dotSize5.get():
            self.dot_size = 5
            self.dotSize1.set(False)
            self.dotSize2.set(False)
            self.dotSize10.set(False)
    #--------------------------------------------------------------------------#
    def dot_size10(self):
        if self.dotSize10.get():
            self.dot_size = 10
            self.dotSize1.set(False)
            self.dotSize2.set(False)
            self.dotSize5.set(False)
    #--------------------------------------------------------------------------#
    def do_something(self):
        print("Menu clicked")
    #--------------------------------------------------------------------------#
    def do_about(self):
        messagebox.showinfo("Help", 'Contact : cochouteo@gmail.com\nVersion  : 1.0.1')
    #--------------------------------------------------------------------------#
    def update(self):

        if hasattr(self,'img'):

            if self.play.get():
                # capture frame
                ret, self.img = self.cap.read()
                if ret:
                    if hasattr(self,'img_crop'):
                        self.img_resize = self.img[self.y1r:self.y2r,self.x1r:self.x2r]
                    else:
                        self.img_resize = self.img.copy()

                    if self.hsvdetection.get() or self.simpleblobdetection.get():
                        self.process()

            if self.do_process:

                if hasattr(self,'img_crop') and not hasattr(self,'cap'):
                    self.img_resize = self.img[self.y1r:self.y2r,self.x1r:self.x2r]
                    if self.drawPoly.get():
                        self.img_resize = self.imgsavepoly[-1][self.y1r:self.y2r,self.x1r:self.x2r]
                elif self.drawPoly.get():
                    self.img_resize = self.imgsavepoly[-1]
                elif not hasattr(self,'cap'):
                    self.img_resize = self.img.copy()
                
                if self.img_resize .shape[0] == 0 or self.img_resize .shape[1] == 0:
                    self.img_resize  = self.img

                if self.simpleblobdetection.get():
                    self.simple_blob_detection(self.img_resize)

                elif self.hsvdetection.get():
                    self.hsv_detection(self.img_resize)
                    
                self.process()

            self.fit_canvas(self.img_resize)

        self.after(self.delay,self.update)
#==============================================================================#
if __name__ == "__main__":
    app = App()
    app.mainloop()